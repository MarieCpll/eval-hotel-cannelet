# Évaluation compétences 1 et 2
Mise en place d'un site pour un hôtel

## Objectifs
* **Créer un prototype des pages "liste des chambres" et "chambre" du site**  
    Nombre de page : 2  
    Résolutions (largeur d'écran) : 320px, 800px, 1200px  
    Compétence : *1 - Maquetter une application*
* **Intégrer les pages "liste des chambres" et "chambre" du site**  
    Nombre de page : 2  
    Résolutions (largeur d'écran) : toutes
    Compétence : *2 - Réaliser une interface utilisateur web statique et adaptable*

## Réalisation :
**Durée :** A rendre avant ce soir minuit <br/>
**Groupe :** En solo  
**Contrainte :** Ne pas utiliser de framework CSS, réaliser 100% du code


## Rendu :
* Un wireframe avec plusieurs écrans aux formats 320px / 800px / 1200px (à minima)
* Le code source du site
* Le lien vers le site en production (surge, heroku...)

## Projet
### Général
#### Header
**Le header contient**
* un logo (n'oubliez pas son utilité)
* un menu (devra se transformer en menu burger) : 
    * Nos chambres (page d'accueil)
    * Nos services
    * Nos tarifs
    * Notre restaurant
        * Infos pratique (sous menu)
        * Le chef (sous menu)
        * La carte (sous menu)
    * Contactez-nous
* un menu pour changer les langues
* le numéro de téléphone

#### Footer
**Le footer contient**
* le logo et l'adresse
* un menu : 
    * Nos chambres (page d'accueil)
    * Nos services
    * Nos tarifs
    * Notre restaurant
    * Contactez-nous
* un abonnement newsletter (n'oubliez pas la totalité des champs)
* le numéro de téléphone, les liens vers les réseaux sociaux (Facebook et Instagram)
* une carte (adresse 55 rue de Bruxelles, 12000 Rodez)
* un copyright et le lien vers la page des mentions légales

### Nos chambres (accueil)
**La page contient**
* un fil d'ariane : accueil
* le titre "Hôtel Cannelet"
* la baseline "Un hôtel simplonien"
* une liste de 12 chambres contenant (pour chacune) : un titre, une image, un bouton "Voir la chambre" (cible du lien : page chambre)
* une section avec les services contenant :
    * le titre "Nos services"
    * 3 blocs de services contenant chacun un titre, une image et une liste de services


### Page chambre chambres (accueil)
**La page contient**
* un fil d'ariane : accueil > chambre chef front
* le titre "Chambre chef front"
* une image
* une description courte
* le nombre de place (2), lit king size
* la salle de bain (douche spa)
* la superficie : 27m2
* l'accès handicapé (oui)
* le prix à la nuitée : 350 €
* un formulaire de réservation
* une section avec les services contenant :
    * le titre "Nos services"
    * 3 blocs de services contenant chacun un titre, une image et une liste de services

## Charte graphique
### Typographie
* Typographie principale : Open Sans
* Typographie secondaire : Merriweather
* Corps de texte : regular|light, 16pt
* Corps de titre : 32pt

### Couleur
* Principale : #3E606F
* Autres : #91AA9D, #D1DBBD, #FCFFF5, #3E606F, #193441, #172329

### Icônes
* logo (logo.svg)
* pictogrammes
